#!/bin/bash

export DEBIAN_FRONTEND='noninteractive'

echo "Mise à jour ubuntu"
apt -y update \
&& apt -y upgrade

echo "Passer le clavier en azerty"
sudo loadkeys fr

echo "Installation de docker"
sudo apt install -y docker.io \
&& sudo usermod -aG docker vagrant

echo "Installation requirement ssh"
sudo apt install -y sshpass

echo "Installation runner"
# docker run -d --name gitlab-runner \
# --restart always \
# -v /var/run/docker.sock:/var/run/docker.sock \
# -v /data/gitlab-runner:/etc/gitlab-runner gitlab/gitlab-runner:latest

docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
  --non-interactive \
  --restart always \
  --executor "docker" \
  --docker-image ubuntu:20.04 \
  --url "https://gitlab.com/" \
  --token "glrt-wrx-PXzeb2-wisXmEaxs" \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --description "docker-runner"


echo "-------------------------------------------------------------------"
echo "Fin des installation du fichier nicocicdrunner_requirements.sh"
echo "-------------------------------------------------------------------"